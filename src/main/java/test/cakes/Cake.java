package test.cakes;

import java.io.Serializable;

public abstract  class Cake implements Serializable{
    private int dimaeter;
    private int height;
    private int wieight;

    void bake(){

    }

    public int getDimaeter() {
        return dimaeter;
    }

    public void setDimaeter(int dimaeter) {
        this.dimaeter = dimaeter;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWieight() {
        return wieight;
    }

    public void setWieight(int wieight) {
        this.wieight = wieight;
    }

    public Cake(int dimaeter, int height, int wieight) {
        this.dimaeter = dimaeter;
        this.height = height;
        this.wieight = wieight;
    }

    public Cake() {
    }
}
