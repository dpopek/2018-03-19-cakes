package test.cakes;

public class BirthdayCake extends Cake{
    private int candlesNumber;

    public int getCandlesNumber() {
        return candlesNumber;
    }

    public void setCandlesNumber(int candlesNumber) {
        this.candlesNumber = candlesNumber;
    }

    @Override
    void bake() {
        System.out.println("Birthday cake is being baked");
    }

    public BirthdayCake(int dimaeter, int height, int wieight, int candlesNumber) {
        super(dimaeter, height, wieight);
        this.candlesNumber = candlesNumber;
    }

    public BirthdayCake() {
    }
}
