package test.cakes;

import java.io.*;

public class CakesMain{
    public static void main(String[] args){
        Cake[] cakes = new Cake[5];

        cakes[0]= new WeddingCake(30, 50, 2 , 3);
        cakes[1]= new WeddingCake(40, 30, 2 , 2);
        cakes[2]= new WeddingCake(60, 60, 5 , 4);
        cakes[3]= new BirthdayCake(25, 20, 1, 9);
        cakes[4]= new BirthdayCake(15, 15, 1, 15);

        bake(cakes);
        System.out.println(countBirthdayCakes(cakes));
        File file = new File("/tmp/cakes");
        save(cakes, file);
    }

    static void bake(Cake[] cakes){
        for (Cake cake : cakes) {
            cake.bake();
        }
    }

    static int countBirthdayCakes(Cake[] cakes){
        int birthdayCakesCout = 0;
        for (Cake cake : cakes) {
            if (cake instanceof BirthdayCake){
                birthdayCakesCout += 1;
                }
        }
        return birthdayCakesCout;
    }

    static void save(Cake[] cakes, File file){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(cakes);

            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
