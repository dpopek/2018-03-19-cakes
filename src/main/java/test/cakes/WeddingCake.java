package test.cakes;

public class WeddingCake extends Cake {
    private int tiersNumber;

    public int getTiersNumber() {
        return tiersNumber;
    }

    public void setTiersNumber(int tiersNumber) {
        this.tiersNumber = tiersNumber;
    }

    @Override
    void bake() {
        System.out.println("Wedding cake is being baked");
    }

    public WeddingCake(int dimaeter, int height, int wieight, int tiersNumber) {
        super(dimaeter, height, wieight);
        this.tiersNumber = tiersNumber;
    }

    public WeddingCake() {
    }
}
